#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
from logging.config import dictConfig

import config as cfg

import get_cases as cases

import get_icu as icu

import get_references as references

dictConfig(cfg.logging_config)
log = logging.getLogger()


def get_all():
    log.info("starting imports")
    references.get_all()
    cases.get_all()
    icu.get_all()
    log.info("finished imports")


if __name__ == "__main__":
    get_all()
