# COVID-19 Data Collector

This project provides scripts to gather references and data from RKI.  
All data is written to InfluxDB.

See `scheduler.py` for scheduled updates.

## Dependencies

`pip install -r requirements.txt`

## Usage

### Local influxdb instance

```
docker run -p 8086:8086 \
      -e INFLUXDB_HTTP_AUTH_ENABLED="true" \
      -e INFLUXDB_ADMIN_USER="admin" \
      -e INFLUXDB_DB="c19d" \
      -e INFLUXDB_USER="influx" \
      -e INFLUXDB_USER_PASSWORD="CorrectHorseBatteryStaple" \
      influxdb:1.8
```

### Collect data

#### Manually
```
python ./get_references.py	# to get all references
python ./get_all.py	# to get all data
```

#### Dockerfile

```
docker build . -t collector
docker run collector
```

Runs `scheduler.py` which updates all references and data daily.

## Data Sources

- [RKI Covid19 Datenpunkte](https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/Covid19_RKI_Sums/FeatureServer/0)
- [RKI Landkreisdaten](https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/RKI_Landkreisdaten/FeatureServer/0)
- [RKI Hospitalisierungsdaten](https://github.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland/)
- [DIVI Tagesreport](https://www.divi.de/register/tagesreport)
