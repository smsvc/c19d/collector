#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
from logging.config import dictConfig

import config as cfg

from influxdb import InfluxDBClient

import pandas as pd

dictConfig(cfg.logging_config)
log = logging.getLogger()

db = InfluxDBClient(**cfg.influx_config)


def __write_to_db(df: pd.DataFrame, measurement: str, tags: dict):
    body = []
    for index, row in df.iterrows():
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": tags,
                "fields": {
                    "ICU_Cases": int(row["faelle_covid_aktuell"]),
                    "ICU_Cases_Last_Week": int(row["7T_Hospitalisierung_Faelle"]),
                    "ICU_Cases_Last_Week_Per_100000": round(
                        float(row["7T_Hospitalisierung_Inzidenz"]), 2
                    ),
                },
            }
        )
    db.write_points(body)


def __get_states(df_rki: pd.DataFrame, df_divi: pd.DataFrame):

    df_rki = df_rki[df_rki.Bundesland_Id != 0]  # remove "de" from DataFrame

    # iterate states
    data = db.query("SELECT * FROM states WHERE type='references'")
    states = pd.DataFrame(data.get_points())

    for index, state in states.iterrows():
        state_id = int(state["ID"])
        state_code = state["Code"]
        state_df = df_rki[df_rki.Bundesland_Id == state_id]

        icu_covid_cases = (
            df_divi[df_divi.bundesland == 5].resample("D").sum().faelle_covid_aktuell
        )

        state_df = state_df.join(icu_covid_cases).fillna(0)

        __write_to_db(
            state_df,
            "states",
            {"state": state_code, "type": "icu"},
        )
    log.info(f"imported {len(states)} state histories")


def __get_country(df_rki: pd.DataFrame, df_divi: pd.DataFrame):

    de_df = df_rki[df_rki.Bundesland_Id == 0]

    icu_covid_cases = df_divi.resample("D").sum().faelle_covid_aktuell

    de_df = de_df.join(icu_covid_cases).fillna(0)

    __write_to_db(
        de_df,
        "de",
        {"type": "icu"},
    )
    log.info("imported country history")


def get_all():
    df_rki = pd.read_csv(
        "https://raw.githubusercontent.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland/master/Aktuell_Deutschland_COVID-19-Hospitalisierungen.csv",  # noqa
        index_col="Datum",
        parse_dates=True,
    )
    df_rki = df_rki[df_rki.Altersgruppe == "00+"]  # only stats for all age groups
    df_rki = df_rki.shift(periods=-1, freq="D")  # shift by one day

    df_divi = pd.read_csv(
        "https://diviexchange.blob.core.windows.net/%24web/zeitreihe-tagesdaten.csv",
        index_col="date",
        parse_dates=True,
    )
    df_divi = df_divi.shift(periods=-1, freq="D")  # shift by one day

    __get_states(df_rki, df_divi)
    __get_country(df_rki, df_divi)


if __name__ == "__main__":
    get_all()
