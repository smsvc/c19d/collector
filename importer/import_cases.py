#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# pylint: disable=missing-module-docstring,missing-function-docstring,invalid-name,line-too-long

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
import zipfile
from logging.config import dictConfig

import config as cfg

import requests

from influxdb import InfluxDBClient

import pandas as pd

dictConfig(cfg.logging_config)
log = logging.getLogger()
LOG_PREFIX = "importing data history for"

db = InfluxDBClient(**cfg.influx_config)


def __write_to_db(df: pd.DataFrame, measurement: str, tags: dict):
    body = []
    for index, row in df.iterrows():
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": {**tags, **{"type": "cases"}},
                "fields": {
                    "Cases": int(row["Cases"]),
                    "Cases_Last_Week": int(row["Cases_Last_Week"]),
                    "Cases_Last_Week_Per_100000": round(
                        float(row["Cases_Last_Week_Per_100000"]), 2
                    ),
                },
            }
        )
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": {**tags, **{"type": "deaths"}},
                "fields": {
                    "Deaths": int(row["Deaths"]),
                    "Deaths_Last_Week": int(row["Deaths_Last_Week"]),
                    "Deaths_Last_Week_Per_100000": round(
                        float(row["Deaths_Last_Week_Per_100000"]), 2
                    ),
                },
            }
        )
    db.write_points(body)


def import_districts():
    log.info("%s districts", LOG_PREFIX)

    data = db.query("SELECT * FROM districts WHERE type='references'")
    districts = pd.DataFrame(data.get_points())

    for district_id in districts["district"]:
        did = str(district_id).zfill(5)
        population = db.query(
            "SELECT Population FROM districts "
            + f"WHERE district='{did}' AND type='references'"
        ).raw["series"][0]["values"][0][1]

        district_history = pd.read_csv(
            f"/tmp/COVID-19-Coronavirus-German-Regions-main/data/de-districts/de-district_timeseries-{did}.tsv",
            sep="\t",
            index_col=0,
            parse_dates=True,
        )
        if district_history.empty:
            continue  # skip empty DataFrames
        district_history["Cases_Last_Week_Per_100000"] = (
            district_history["Cases_Last_Week"] / population * 100000
        )
        district_history["Deaths_Last_Week_Per_100000"] = (
            district_history["Deaths_Last_Week"] / population * 100000
        )
        __write_to_db(
            district_history,
            "districts",
            {"district": did},
        )


def import_states():
    log.info("%s states", LOG_PREFIX)

    data = db.query("SELECT * FROM states WHERE type='references'")
    states = pd.DataFrame(data.get_points())

    for state_code in states["Code"]:
        population = db.query(
            "SELECT Population FROM states "
            + f"WHERE state='{state_code}' AND type='references'"
        ).raw["series"][0]["values"][0][1]

        state_history = pd.read_csv(
            f"/tmp/COVID-19-Coronavirus-German-Regions-main/data/de-states/de-state-{state_code}.tsv",
            sep="\t",
            index_col=0,
            parse_dates=True,
        )
        if state_history.empty:
            continue  # skip empty DataFrames
        state_history["Cases_Last_Week_Per_100000"] = (
            state_history["Cases_Last_Week"] / population * 100000
        )
        state_history["Deaths_Last_Week_Per_100000"] = (
            state_history["Deaths_Last_Week"] / population * 100000
        )
        __write_to_db(
            state_history,
            "states",
            {"state": state_code},
        )


def import_country():
    log.info("%s country", LOG_PREFIX)

    de_history = pd.read_csv(
        "/tmp/COVID-19-Coronavirus-German-Regions-main/data/de-states/de-state-DE-total.tsv",
        sep="\t",
        index_col=0,
        parse_dates=True,
    )
    if de_history.empty:
        return  # skip empty DataFrames

    population = db.query(
        "SELECT sum(Population) FROM districts WHERE type='references'"
    ).raw["series"][0]["values"][0][1]

    de_history["Cases_Last_Week_Per_100000"] = (
        de_history["Cases_Last_Week"] / population * 100000
    )
    de_history["Deaths_Last_Week_Per_100000"] = (
        de_history["Deaths_Last_Week"] / population * 100000
    )
    __write_to_db(
        de_history,
        "de",
        {"type": "cases"},
    )


def get_data_archive():
    log.info("downloading archive data")
    data = requests.get(
        "https://github.com/entorb/COVID-19-Coronavirus-German-Regions/archive/refs/heads/main.zip",
        timeout=60,
    )

    with open("/tmp/entorb.zip", "wb") as f:
        f.write(data.content)

    with zipfile.ZipFile("/tmp/entorb.zip", "r") as zip_ref:
        zip_ref.extractall("/tmp/")


def import_all():
    get_data_archive()
    import_districts()
    import_states()
    import_country()


if __name__ == "__main__":
    import_all()
