#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# pylint: disable=missing-module-docstring,missing-function-docstring,invalid-name,line-too-long

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from datetime import datetime
import logging
from logging.config import dictConfig

import config as cfg

from influxdb import InfluxDBClient

import pandas as pd

dictConfig(cfg.logging_config)
log = logging.getLogger()
LOG_PREFIX = "importing icu history for"

db = InfluxDBClient(**cfg.influx_config)

# see https://de.wikipedia.org/wiki/                                                  Amtlicher_Gemeindeschl%C3%BCssel#L%C3%A4nder_der_Bundesrepublik_Deutschland_(bis_1949_L%C3%A4nder_der_Besatzungszonen)   # noqa
ID2CODE = {
    1: "SH",
    2: "HH",
    3: "NI",
    4: "HB",
    5: "NW",
    6: "HE",
    7: "RP",
    8: "BW",
    9: "BY",
    10: "SL",
    11: "BE",
    12: "BB",
    13: "MV",
    14: "SN",
    15: "ST",
    16: "TH",
}


def __write_to_db(df: pd.DataFrame, measurement: str, tags: dict):
    body = []
    for index, row in df.iterrows():
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": tags,
                "fields": {
                    "ICU_Cases": int(row["faelle_covid_aktuell"]),
                    "ICU_Cases_Last_Week": int(row["7T_Hospitalisierung_Faelle"]),
                    "ICU_Cases_Last_Week_Per_100000": round(
                        float(row["7T_Hospitalisierung_Inzidenz"]), 2
                    ),
                },
            }
        )
    db.write_points(body)


def import_states(df_rki: pd.DataFrame, df_divi: pd.DataFrame):
    log.info("%s states", LOG_PREFIX)

    df_rki = df_rki[df_rki.Bundesland_Id != 0]  # remove "de" from DataFrame

    # iterate states
    for state_id in df_rki.Bundesland_Id.unique():
        state_code = ID2CODE[state_id]
        state_df = df_rki[df_rki.Bundesland_Id == state_id]

        icu_covid_cases = (
            df_divi[df_divi.bundesland == 5].resample("D").sum().faelle_covid_aktuell
        )

        state_df = state_df.join(icu_covid_cases).fillna(0)

        __write_to_db(
            state_df,
            "states",
            {"state": state_code, "type": "icu"},
        )


def import_country(df_rki: pd.DataFrame, df_divi: pd.DataFrame):
    log.info("%s country", LOG_PREFIX)

    de_df = df_rki[df_rki.Bundesland_Id == 0]

    icu_covid_cases = df_divi.resample("D").sum().faelle_covid_aktuell

    de_df = de_df.join(icu_covid_cases).fillna(0)

    __write_to_db(
        de_df,
        "de",
        {"type": "icu"},
    )


def import_all():
    maxdate = datetime(2023, 6, 1)

    df_rki = pd.read_csv(
        "https://raw.githubusercontent.com/robert-koch-institut/COVID-19-Hospitalisierungen_in_Deutschland/master/Aktuell_Deutschland_COVID-19-Hospitalisierungen.csv",  # noqa
        index_col="Datum",
        parse_dates=True,
    )

    df_rki = df_rki[df_rki.Altersgruppe == "00+"]  # only stats for all age groups
    df_rki = df_rki.shift(periods=-1, freq="D")  # shift by one day

    df_rki = df_rki.loc[df_rki.index <= maxdate]

    df_divi = pd.read_csv(
        "https://diviexchange.blob.core.windows.net/%24web/zeitreihe-tagesdaten.csv",
        index_col="date",
        parse_dates=True,
    )

    df_divi = df_divi.shift(periods=-1, freq="D")  # shift by one day

    df_divi = df_divi.loc[df_divi.index <= maxdate]

    import_states(df_rki, df_divi)
    import_country(df_rki, df_divi)


if __name__ == "__main__":
    import_all()
