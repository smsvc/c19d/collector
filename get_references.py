#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
from logging.config import dictConfig

import config as cfg

from influxdb import InfluxDBClient

import pandas as pd

import requests

# see https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel#L%C3%A4nder_der_Bundesrepublik_Deutschland_(bis_1949_L%C3%A4nder_der_Besatzungszonen)   # noqa
ID2CODE = {
    1: "SH",
    2: "HH",
    3: "NI",
    4: "HB",
    5: "NW",
    6: "HE",
    7: "RP",
    8: "BW",
    9: "BY",
    10: "SL",
    11: "BE",
    12: "BB",
    13: "MV",
    14: "SN",
    15: "ST",
    16: "TH",
}

dictConfig(cfg.logging_config)
log = logging.getLogger()

db = InfluxDBClient(**cfg.influx_config)


def __get_districts(data: list):
    body = []

    districts = data[
        ["BL", "BL_ID", "EWZ_BL", "GEN", "BEZ", "RS", "EWZ"]
    ].drop_duplicates()
    for index, district in districts.iterrows():
        state_code = ID2CODE[int(district["BL_ID"])]
        district_id = str(district["RS"]).zfill(5)
        body.append(
            {
                "time": "1970-01-01",
                "measurement": "districts",
                "tags": {
                    "district": district_id,
                    "type": "references",
                },
                "fields": {
                    "Population": district["EWZ"],
                    "BL_Name": district["BL"],
                    "BL_Code": state_code,
                    "LK_Name": district["GEN"],
                    "LK_Typ": district["BEZ"],
                },
            }
        )
    db.write_points(body)
    log.info(f"imported {len(districts)} district references")


def __get_states(data: pd.DataFrame):
    body = []

    states = data[["BL", "BL_ID", "EWZ_BL"]].drop_duplicates()
    for index, state in states.iterrows():
        state_code = ID2CODE[int(state["BL_ID"])]
        body.append(
            {
                "time": "1970-01-01",
                "measurement": "states",
                "tags": {
                    "state": state_code,
                    "type": "references",
                },
                "fields": {
                    "Population": state["EWZ_BL"],
                    "State": state["BL"],
                    "Code": state_code,
                    "ID": state["BL_ID"],
                    "Pop Density": -1,
                },
            }
        )
    db.write_points(body)
    log.info(f"imported {len(states)} state references")


def __fetch_rki_data() -> pd.DataFrame:
    res = requests.get(
        url="https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query",  # noqa
        params={
            "where": "1=1",
            "outFields": "BL, BL_ID, EWZ_BL, GEN, BEZ, RS, EWZ",
            "returnGeometry": "false",
            "f": "json",
        },
    )
    series = [el["attributes"] for el in res.json()["features"]]
    series_df = pd.DataFrame(series)
    return series_df


def get_all():
    rki_data = __fetch_rki_data()
    __get_districts(rki_data)
    __get_states(rki_data)


if __name__ == "__main__":
    get_all()
