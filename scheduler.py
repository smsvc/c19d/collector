#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
import time
from logging.config import dictConfig

import config as cfg

import get_all as fetchers

from influxdb import InfluxDBClient

import schedule

dictConfig(cfg.logging_config)
log = logging.getLogger()

# schedule references and data collection
schedule.every().day.at(cfg.update_time).do(fetchers.get_all)

# initially get data and reference
db = InfluxDBClient(**cfg.influx_config)
data = db.query("SELECT COUNT(Population) FROM districts WHERE type = 'references'")
if len(data.raw["series"]) == 0:
    log.info("Database empty - starting import")
    fetchers.get_all()

log.info("Scheduler started")
while True:
    schedule.run_pending()
    time.sleep(1)
