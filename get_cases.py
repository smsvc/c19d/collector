#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import logging
from logging.config import dictConfig

import config as cfg

from influxdb import InfluxDBClient

import pandas as pd

import requests

dictConfig(cfg.logging_config)
log = logging.getLogger()

db = InfluxDBClient(**cfg.influx_config)


def __write_to_db(df: pd.DataFrame, measurement: str, tags: dict):
    body = []
    for index, row in df.iterrows():
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": {**tags, **{"type": "cases"}},
                "fields": {
                    "Cases": int(row["Cases"]),
                    "Cases_Last_Week": int(row["Cases_Last_Week"]),
                    "Cases_Last_Week_Per_100000": float(
                        row["Cases_Last_Week_Per_100000"]
                    ),
                },
            }
        )
        body.append(
            {
                "time": index.strftime("%Y-%m-%d"),
                "measurement": measurement,
                "tags": {**tags, **{"type": "deaths"}},
                "fields": {
                    "Deaths": int(row["Deaths"]),
                    "Deaths_Last_Week": int(row["Deaths_Last_Week"]),
                    "Deaths_Last_Week_Per_100000": float(
                        row["Deaths_Last_Week_Per_100000"]
                    ),
                },
            }
        )
    db.write_points(body)


def __fetch_rki_sums() -> pd.DataFrame:
    series = []
    offset = 0

    # query rki api
    allDataFetched = False
    while not allDataFetched:
        res = requests.get(
            url="https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/Covid19_RKI_Sums/FeatureServer/0/query",  # noqa
            params={
                "where": "1=1",
                "outFields": "Meldedatum,SummeFall,SummeTodesfall,AnzahlFall,AnzahlTodesfall,IdBundesland,IdLandkreis",  # noqa
                "orderByFields": "Meldedatum",
                "resultOffset": offset,
                "f": "json",
            },
        )

        # extract timeseries elements
        for element in res.json()["features"]:
            series.append(element["attributes"])

        # more data waiting?
        try:
            res.json()["exceededTransferLimit"]
        except KeyError:
            allDataFetched = True
        else:
            offset += 20000

    series_df = pd.DataFrame(series)
    series_df["Meldedatum"] = pd.to_datetime(series_df["Meldedatum"] / 1000, unit="s")
    series_df.rename(
        columns={
            "Meldedatum": "Date",
            "SummeFall": "Cases",
            "SummeTodesfall": "Deaths",
        },
        inplace=True,
    )
    log.info(f"fetched {len(series_df)} rki datapoints")
    return series_df


def __process_history(history: pd.DataFrame, population: int) -> pd.DataFrame:
    # prepare dataframe
    history = history.groupby("Date").sum()
    history.shift(periods=-1, freq="D")

    # calc incidence
    history["Cases_Last_Week"] = history["AnzahlFall"].rolling(7).sum()
    history["Cases_Last_Week_Per_100000"] = round(
        history["Cases_Last_Week"] / population * 100000, 2
    )

    # calc deaths
    history["Deaths_Last_Week"] = history["AnzahlTodesfall"].rolling(7).sum()
    history["Deaths_Last_Week_Per_100000"] = round(
        history["Deaths_Last_Week"] / population * 100000, 2
    )

    # fill empty values
    history.fillna(0, inplace=True)

    return history


def __process_districts(rki_sums: pd.DataFrame):
    data = db.query("SELECT * FROM districts WHERE type='references'")
    districts = pd.DataFrame(data.get_points())

    for district_id in districts["district"]:
        did = str(district_id).zfill(5)
        population = db.query(
            "SELECT Population FROM districts "
            + f"WHERE district='{did}' AND type='references'"
        ).raw["series"][0]["values"][0][1]

        district_rki_sums = rki_sums[rki_sums.IdLandkreis == did]
        district_history = __process_history(district_rki_sums, population)

        __write_to_db(
            district_history,
            "districts",
            {"district": did},
        )
    log.info(f"imported {len(districts)} district histories")


def __process_states(rki_sums: pd.DataFrame):
    data = db.query("SELECT * FROM states WHERE type='references'")
    states = pd.DataFrame(data.get_points())

    for index, state in states.iterrows():
        state_id = int(state["ID"])
        state_code = state["Code"]

        population = db.query(
            "SELECT Population FROM states "
            + f"WHERE ID='{state_id}' AND type='references'"
        ).raw["series"][0]["values"][0][1]

        state_rki_sums = rki_sums[rki_sums.IdBundesland == state_id]
        state_history = __process_history(state_rki_sums, population)

        __write_to_db(
            state_history,
            "states",
            {"state": state_code},
        )
    log.info(f"imported {len(states)} state histories")


def __process_country(rki_sums):

    population = db.query(
        "SELECT sum(Population) FROM districts WHERE type='references'"
    ).raw["series"][0]["values"][0][1]

    de_history = __process_history(rki_sums, population)

    __write_to_db(
        de_history,
        "de",
        {"type": "cases"},
    )
    log.info("imported country history")


def get_all():
    rki_sums = __fetch_rki_sums()
    __process_districts(rki_sums)
    __process_states(rki_sums)
    __process_country(rki_sums)


if __name__ == "__main__":
    get_all()
